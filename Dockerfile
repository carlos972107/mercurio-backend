FROM node:12

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install -g node-gyp \
    && npm install -g nodemon \
    && npm install --quiet

COPY . .

EXPOSE 4004