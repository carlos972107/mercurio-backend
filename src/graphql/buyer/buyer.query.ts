'use strict';

import { model } from '../../models';
import to from 'await-to-js';

export const Query = {
    getBuyer: async (_, {}, { user }) => {
        let err, res;
        [err, res] = await to(model.Buyer.findOne({ _id: user._id }));
        if(err) throw err;
        return res;
    }
}