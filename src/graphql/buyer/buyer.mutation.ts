'use strict';

import { model } from '../../models';
import to from 'await-to-js';
import { sendEmail, generatePassword } from '../../service';

export const Mutation = {
    createBuyer: async (_, { data }) => {
        let err, res;
        [err, res] = await to(model.Buyer.create(data));
        if(err) throw err;
        return res;
    },
    updateBuyer: async (_ , { data, _id }) => {
        let err, res;
        [err, res] = await to(model.Buyer.findByIdAndUpdate(_id, data));
        if(err) throw err;
        return res;
    },
    deleteBuyer: async (_, { _id }) => {
        let err, res;
        [err, res] = await to(model.Buyer.findByIdAndRemove(_id));
        if(err) throw err;
        return res;
    },
    updatePasswordBuyer: async (_, { password }, { user }) => {
        let err, res;
        [err, res] = await to(model.Buyer.findOneAndUpdate( { _id: user._id }, { password }, { isModified: true }));
        if(err) throw err;
        return res;
    },
    loginBuyer: async (_, { email, password }) => {
        let err, buyer;
        [err, buyer] = await to(model.Buyer.findOne({ email }).exec());
        if(!buyer) throw `no existe ningun usuario con correo ${email}`;
        if(err) throw err;
        let valid = await buyer.comparePassword(password);
        if(!valid) throw `Contraseña invalida`;
        else return await buyer.token(buyer);
    },
    forgotPasswordBuyer: async (_, { email }) => {
        let err, res;
        [err, res] = await to(model.Buyer.findOne({ email }).exec());
        if(err) throw err;
        if(!res) throw `El correo ${email} no está en una cuenta relacionada en mercurio`;
        const password = generatePassword();
        await model.Buyer.findOneAndUpdate({ _id: res._id }, { password }, { isModified: true }).exec();
        sendEmail(email, 'Recuperacion de contraseña',  `Hola!, ${res.name} tu nueva contraseña para el ingreso a ¿Quién Vende? es ${password}`);
        return res;
    }
}