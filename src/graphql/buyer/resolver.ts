'use strict';

import { Query } from './buyer.query';
import { Mutation } from './buyer.mutation';

export const resolver = {
    Query,
    Mutation
}