'use strict';

import { Query } from './seller.query';
import { Mutation } from './seller.mutation';

export const resolver = {
    Query,
    Mutation
}