'use strict';

import { model } from '../../models';
import to from 'await-to-js';

export const Mutation = {
    createSeller: async (_, { data }) => {
        let err, res;
        [err, res] = await to(model.Seller.create(data));
        if(err) throw err;
        return res;
    },
    updateSeller: async (_, { data, _id }) => {
        let err, res;
        [err, res] = await to(model.Seller.findByIdAndUpdate(_id, data));
        if(err) throw err;
        return res;
    },
    deleteSeller: async (_, { _id }) => {
        let err, res;
        [err, res] = await to(model.Seller.findByIdAndRemove(_id));
        if(err) throw err;
        return res;
    },
    updatePasswordSeller: async (_, { password }, { user }) => {
        let err, res;
        [err, res] = await to(model.Seller.findOneAndUpdate( { _id: user._id }, { password }, { isModified: true }));
        if(err) throw err;
        return res;
    },
    loginSeller: async (_, { email, password }) => {
        let err, seller;
        [err, seller] = await to(model.Seller.findOne({ email }).exec());
        if(!seller) throw 'No existe ningun usuario con este correo';
        if(err) throw err;
        let valid = await seller.comparePassword(password);
        if(!valid) throw `Contraseña invalida`;
        else return await seller.token(seller);
    }
}