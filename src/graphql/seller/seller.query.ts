'use strict';

import { model } from '../../models';
import to from 'await-to-js';

export const Query = {
    getSeller: async (_ , {}, { user }) => {
        let err, res;
        [err, res] = await to(model.Seller.findOne({ _id : user._id })/*.autopopulate()*/);
        if(err) throw err;
        return res;
    },
    getSellerAll: async () => {
        let err, res;
        [err, res] = await to(model.Seller.find({}));
        if(err) throw err;
        return res;
    },
    searchSeller: async (_, { search }) => {
        let err, res;
        [err, res] = await to(model.Seller.find(search ? {
            $or: [
                { "businessName": new RegExp(search, "gi") },
            ]
        } : {}).sort({'createAt': 'asc'}));
        if(err) throw err;
        return res;
    }
}