import { IsAuthDirective } from './auth.directive';
import { HasRoleDirective } from './role.directive';

export const schemaDirectives = {
    isAuth: IsAuthDirective,
    //isAuthUser: IsAuthUserDirective,
    isRole: HasRoleDirective
};
