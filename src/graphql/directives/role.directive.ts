import { SchemaDirectiveVisitor } from "apollo-server-express";
import { defaultFieldResolver } from "graphql";

export class HasRoleDirective extends SchemaDirectiveVisitor {
    visitFieldDefinition(field) {
      const { resolve = defaultFieldResolver } = field;
      const { roles } = this.args;
      field.resolve = async function(...args) {
        let rol;
        [, , { user: rol }] = args;
        let userRoles = rol.roles;
        if (roles.some(role => userRoles.indexOf(role) !== -1)) {
          const result = await resolve.apply(this, args);
          return result;
        }
        throw new Error( "You are not authorized for this resource" );
      };
    }
  }