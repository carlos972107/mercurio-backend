'use strict';

import { model } from '../../models';
import to from 'await-to-js';

export const Query = {
    getProduct: async (_, { _id }) => {
        let err, res;
        [err, res] = await to(model.Product.findOne({ _id }).autopopulate());
        if(err) throw err;
        return res;
    },
    getProductsAll: async (_, {}) => {
        let err, res;
        [err, res] = await to(model.Product.find({}).autopopulate());
        if(err) throw err;
        return res;
    },
    getProductsSeller: async (_, {}, { user }) => {
        let err, res;
        [err, res] = await to(model.Product.find({ seller: user._id }).autopopulate());
        if(err) throw err;
        return res;
    },
    getProductsCategories: async (_, { _id }) => {
        let err, res;
        [err, res] = await to(model.Product.find({ category: _id }).autopopulate());
        if(err) throw err;
        return res;
    }
}