'use strict';

import { model } from '../../models';
import to from 'await-to-js';

export const Mutation = {
    createProduct: async (_, { data }, { user }) => {
        data.seller = user._id;
        let err, res;
        [err, res] = await to(model.Product.create(data));
        if(err) throw err;
        return res;
    },
    updateProduct: async (_, { data, _id }) => {
        let err, res;
        [err, res] = await to(model.Product.findByIdAndUpdate(_id, data));
        if(err) throw err;
        return res;
    },
    deleteProduct: async (_, { _id }) => {
        let err, res;
        [err, res] = await to(model.Product.findOneAndDelete({_id}));
        if(err) throw err;
        return res;
    }
}