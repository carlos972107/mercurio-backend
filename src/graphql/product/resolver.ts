'use strict';

import { Query } from './product.query';
import { Mutation } from './product.mutation';

export const resolver = {
    Query,
    Mutation
}