'use strict';

import { model } from '../../models';
import to from 'await-to-js';

export const Mutation = {
    createTokenNotification: async (_, { token }, { user }) => {
        const data = { token, idIdentity: user._id };
        let err, res;
        [err, res] = await to(model.ExpoTokenNotification.create(data));
        if(err) throw err;
        return res;
    }
}