'use strict';

import { model } from '../../models';
import to from 'await-to-js';

export const Query = {
    getRequestOrder: async (_ , { _id }) => {
        let err, res;
        [err, res] = await to(model.RequestOrder.findOne({ _id }));
        if(err) throw err;
        return res;
    },
    getRequestOrderAll: async (_, { search }, { user }) => {
        let err, res;
        [err, res] = await to(model.RequestOrder.find(search ? {
            $or: [
                { "code": new RegExp(search, "gi") },
            ]
        } : { $or: [{ "seller": user._id }, { "buyer": user._id }] }));
        if(err) throw err;
        return res;
    }
}