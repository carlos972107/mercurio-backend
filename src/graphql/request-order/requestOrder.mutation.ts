'use strict';

import { model } from '../../models';
import to from 'await-to-js';

export const Mutation = {
    createRequestOrder: async (_, { data }, { user }) => {
        data.buyer = user._id;
        let err, res;
        [err, res] = await to(model.RequestOrder.create(data));
        if(err) throw err;
        return res;
    },
    updateRequestOrder: async (_, { data, _id }) => {
        let err, res;
        [err, res] = await to(model.RequestOrder.findByIdAndUpdate(_id, data));
        if(err) throw err;
        return res;
    },
    deleteRequestOrder: async (_, { _id }) => {
        
    }
}