'use strict';

import { Query } from './requestOrder.query';
import { Mutation } from './requestOrder.mutation';

export const resolver = {
    Query,
    Mutation
}