'use strict';

import { model } from '../../models';
import to from 'await-to-js';

export const Query = {
    getCategory: async (_, { _id }) => {
        let err, res;
        [err, res] = await to(model.Category.findOne({ _id }).autopopulate());
        if(err) throw err;
        return res;
    },
    getCategoryAll: async (_, { _id = '' }, { user }) => {
        let err, res;
        [err, res] = await to(model.Category.find({ $or: [{ seller: user._id }, { seller: _id }]}).autopopulate());
        if(err) throw err;
        return res;
    },
    getCategoriesAllSeller: async (_, {}, { user }) => {
        let err, res;
        [err, res] = await to(model.Category.find({ seller: user._id }).autopopulate());
        if(err) throw err;
        return res;
    },
    getCategoriesSeller: async (_, { _id }) => {
        let err, res;
        [err, res] = await to(model.Category.find({ seller: _id }).autopopulate());
        if(err) throw new Error(err);
        return res;
    }
}