'use strict';

import { Query } from './category.query';
import { Mutation } from './category.mutation';

export const resolver = {
    Query,
    Mutation
};