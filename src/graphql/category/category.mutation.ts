'use strict';

import { model } from '../../models';
import to from 'await-to-js';

export const Mutation = {
    createCategory: async (_, { data }, { user }) => {
        const payload = data;
        payload.seller = user._id;
        let err, res;
        [err, res] = await to(model.Category.create(payload));
        if(err) throw err;
        return res;
    },
    updateCategory: async (_, { data, _id }) => {
        let err, res;
        [err, res] = await to(model.Category.findByIdAndUpdate(_id, data));
        if(err) throw err;
        return res;
    },
    deleteCategory: async (_, { _id }) => {
        let err, res;
        [err, res] = await to(model.Category.findByIdAndRemove(_id));
        if(err) throw err;
        return res;
    }
}