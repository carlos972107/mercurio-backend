'use strict';

import * as dotenv from 'dotenv';
dotenv.config();

export const ENV = {
    JWT_ENCRYPTION: process.env.JWT_ENCRYPTION || '2020segure97yTjwtBcryp',
    JWT_EXPIRATION: process.env.JWT_EXPIRATION || '1y',
    MONGODB_URI: process.env.MONGODB_URI,
    PORT: process.env.PORT || '4004',
    NODEMAILERAUTH: process.env.nodemailerAuth,
    NODEMAILERPASS: process.env.nodemailerPass
}