'use strict';

import * as nodemailer from 'nodemailer';
import { ENV } from '../config';

export const sendEmail = (to, subject, text) => {
    const transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        auth: {
            type: "login",
            user: ENV.NODEMAILERAUTH,
            pass: ENV.NODEMAILERPASS
        }
    });

    const credentials = {
        from: ENV.NODEMAILERAUTH,
        to,
        subject,
        text
    }
    transporter.sendMail(credentials, function(error, info){
        if (error){
            console.log(error);
        } else {
            console.log("Email sent");
        }
    });
};

export const generatePassword = () => {
    let caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHJKMNPQRTUVWXYZ2346789";
    let password = "";
    for (let i = 0; i < 20; i++) password += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
    return password;
}