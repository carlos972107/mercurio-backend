'use strict';

import * as express  from 'express';
import * as JWT from 'express-jwt';
import * as cors from 'cors';
import { ApolloServer } from 'apollo-server-express';
import { databaseConnection } from './models';
import { ENV } from './config';
import { Auth } from './middleware';
import { resolver as resolvers, schema, schemaDirectives } from './graphql';

const app = express();
const corsOptions = {
  origin: '*',
  credentials: true // <-- REQUIRED Backend Setting
};
app.use(cors(corsOptions));

const authMiddleware = JWT({
    secret: ENV.JWT_ENCRYPTION,
    credentialsRequired: false,
});

app.use(authMiddleware);

const server = new ApolloServer({
    typeDefs: schema,
    resolvers,
    playground: true,
    schemaDirectives,
    context: ( { req, res } ) => {
        const user = Auth(req);
        return {
            user,
            req,
            res,
        };
    }
});

server.applyMiddleware({
  app
});

app.listen({ port: ENV.PORT }, async () => {
    console.log(`🚀 Server ready at http://localhost:${ENV.PORT}${server.graphqlPath}`);
    await databaseConnection();
});
