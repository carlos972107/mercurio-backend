'use strict';

import express from 'express';
import * as jwt from 'jsonwebtoken';
import { ENV } from '../config';

export const Auth = (req: express.Request): string | object => {
  const token = req.headers.authorization || '';
  const SECRET = ENV.JWT_ENCRYPTION;

  if (token) {
    const tokenValue = token.replace('Bearer ', '');
    const user = jwt.verify(tokenValue, SECRET, ['HS512']);
    return user;
  }

  return null;
};