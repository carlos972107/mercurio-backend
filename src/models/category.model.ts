'use strict';

import { Schema, Document, model } from 'mongoose';

const { ObjectId } = Schema.Types;

const CategorySchema = new Schema({
    name: {
        type: String,
        required: true
    },
    photo_url: {
        type: String
    },
    description: {
        type: String,
        required: true
    },
    seller: {
        type: ObjectId,
        ref: 'Seller'
    },
    createAt: {
        type: String,
        default: new Date().toLocaleString()
    },
    updateAt: {
        type: String,
        default: new Date().toLocaleString()
    }
});

CategorySchema.query.autopopulate = function(){
    this.populate('seller');
    return this;
};

export const Category = model('Category', CategorySchema);