'use strict';

import {Schema, Document, model} from 'mongoose';
import * as bcrypt from 'bcrypt';
import * as jsonwebtoken from'jsonwebtoken';
import { ENV } from '../config';
const emailRegex = /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i;

/*const discriminatorkey = 'user'
const salt = 10;*/

const { Mixed, ObjectId } = Schema.Types;

export interface IUser extends Document{
    businessName: string,
    email: string,
    password: string,
    role: string,
    state: string,
    comparePassword(password:string): boolean
    token(seller): string
}

const SellerUserSchema = new Schema({
    businessName: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        validate: {
            validator: (v) => {
              return emailRegex.test(v);
            },
            message: 'Correo electrónico no válido'
        }
    },
    photo_url: {
        type: String
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true
    },
    address: {
        lat: String,
        lng: String,
        zoom: Number
    },
    addressMaps: Mixed,
    initDate: {
        type: String,
        required: true
    },
    endDate: {
        type: String,
        required: true
    },
    phone: {
        type: String
    },
    mobile: [
        {
            type: String,
            required: true
        }
    ],
    /*coverage: [
        {
            type: ObjectId,
            ref: 'Coverage'
        }
    ],*/
    priority: {
        type: Number
    },
    paymentMethods: [
        {
            type: String
        }
    ],
    /*ranking: {
        type: ObjectId,
        ref: 'Ranking'
    }*/
    createAt: {
        type: String,
        default: new Date().toLocaleString()
    },
    updateAt: {
        type: String,
        default: new Date().toLocaleString()
    }
});

SellerUserSchema.pre('save',async function(next){
    const self = this as IUser;

    if (!this.isModified('password')) {
        return next();
    }
    
    const doc = await this.constructor.find({
        email: self.email
    }).exec();

    if(doc.length) throw 'error correo en uso';

    try{
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(self.password, salt);
        self.password = hash;
        return next(null, this);
    }catch(err){
        throw err;
    }
});

SellerUserSchema.pre(['findByIdAndUpdate', 'findOneAndUpdate'], async function(next){
    const self = this.getUpdate();
    self.updateAt = new Date().toLocaleString();
    if (self.password) {
        try{
            const salt = await bcrypt.genSalt(10);
            const hash = await bcrypt.hash(self.password, salt);
            self.password = hash;
            return next(null, this);
        }catch(err){
            throw err;
        }
    }
});

SellerUserSchema.methods.comparePassword = function(candidatePassword){
    return bcrypt.compare(candidatePassword, this.password);
};

SellerUserSchema.methods.token = function(seller){
    const payload = seller;
    return 'Bearer ' + jsonwebtoken.sign(payload.toJSON(),
        ENV.JWT_ENCRYPTION, { expiresIn: ENV.JWT_EXPIRATION });
}

/*SellerUserSchema.query.autopopulate = function(){
    this.populate('Coverage');
    return this;
}*/

export const Seller = model<IUser>('Seller', SellerUserSchema);