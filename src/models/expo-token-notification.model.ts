'use strict';

import { Schema, model, Document } from 'mongoose';

export interface IExpoToken extends Document{
    token: string,
    idIdentity: string
}

const ExpoTokenNotificationSchema = new Schema({
    token: {
        type: String,
        required: true
    },
    idIdentity: {
        type: String,
        required: true
    },
    createAt: {
        type: String,
        default: new Date().toLocaleString()
    },
    updateAt: {
        type: String,
        default: new Date().toLocaleString()
    }
});

ExpoTokenNotificationSchema.pre('save', async function(){
    const self = this as IExpoToken;
    const doc = await this.constructor.find({
        idIdentity: self.idIdentity
    }).exec();
    if(doc) throw new Error('token con existencia');
});

export const ExpoTokenNotification = model('ExpoTokenNotification', ExpoTokenNotificationSchema);