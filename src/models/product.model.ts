'use strict';

import { Schema, model, Document } from 'mongoose';

const { ObjectId } = Schema.Types;

const ProductSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    photo_url: {
        type: String
    },
    stoktaking: {
        type: Number,
        required: true
    },
    visible: {
        type: Boolean,
        required: true,
        default: false
    },
    seller: {
        type: ObjectId,
        ref: 'Seller'
    },
    category: {
        type: ObjectId,
        ref: 'Category'
    },
    createAt: {
        type: String,
        default: new Date().toLocaleString()
    },
    updateAt: {
        type: String,
        default: new Date().toLocaleString()
    }
});

ProductSchema.query.autopopulate = function(){
    this.populate('seller');
    this.populate('category');
    return this;
}

export const Product = model('Product', ProductSchema);