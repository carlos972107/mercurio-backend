'use strict';

import { model } from '../models';
import { Schema, model as Model, Document } from 'mongoose';
import { Expo } from 'expo-server-sdk';
import { v4 as uuidv4 } from 'uuid';
const expo = new Expo();

const { ObjectId, Mixed } = Schema.Types;

const RequestOrderSchema = new Schema({
    code: {
        type: String,
        required: true
    },
    product: [
        {
            type: ObjectId,
            ref: 'Product'
        }
    ],
    seller: {
        type: ObjectId,
        ref: 'Seller'
    },
    buyer: {
        type: ObjectId,
        ref: 'Buyer'
    },
    /*requestTransport: {
        tye: ObjectId,
        ref: 'RequestTransport'
    },*/
    total: {
        type: Number,
        required: true
    },
    priceTransport: {
        type: Number,
        required: true
    },
    description: {
        type: String
    },
    destination: {
        type: String
    },
    origin: {
        type: String
    },
    destinationMap: {
        lat: String,
        lng: String,
        zoom: Number
    },
    originMaps: {
        lat: String,
        lng: String,
        zoom: Number
    },
    state: {
        type: String,
        required: true
    },
    paymentMethod: {
        type: String,
        required: true
    },
    createAt: {
        type: String,
        default: new Date().toLocaleString()
    },
    updateAt: {
        type: String,
        default: new Date().toLocaleString()
    }
});

RequestOrderSchema.pre('save', async function(next){
    const self = this;
    self.code = uuidv4();
    
    const result = await model.ExpoTokenNotification.find({
        idIdentity: {
            $in: [self.buyer, self.seller]
        }
    }).exec();

    let notifications = [];
    for(let pushToken of result){
        if (!Expo.isExpoPushToken(pushToken)) {
            console.error(`Push token ${pushToken} is not a valid Expo push token`);
            return next();
        }
        let title = 'Pedido creado';
        let body = `Nuevo pedido creado con codigo ${self.code}.`;
        notifications.push({
            to: pushToken,
            sound: "default",
            title: title,
            body: body,
            data: { body }
        });
    }
    let chunks = expo.chunkPushNotifications(notifications);
    (async () => {
        for(let chunk of chunks){
            try {
                let receipts = await expo.sendPushNotificationsAsync(chunk);
                console.log(receipts);
            } catch (error) {
                console.error(error);
            }
        }
    })();
    return next(null, this);
});

RequestOrderSchema.query.autopopulate = function(){
    this.populate('product');
    this.populate('seller');
    this.populate('buyer');
    return this;
}

export const RequestOrder = Model('RequestOrder', RequestOrderSchema);