'use strict';

import * as mongoose from 'mongoose';
import { ENV } from '../config';

export const databaseConnection = async () => {
    /**
     * @connection
     * connection Mongodb
     */
    await mongoose.connect(ENV.MONGODB_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }, err => {
        if(err)
            console.log(`Fallo la conexion con mongoose ${err}`);
        else
            console.log(`🚀 Conexion exitosa con mongoose!.`);
    });
}

import { Buyer } from './buyer.model';
import { Seller } from './seller.model';
import { Product } from './product.model';
import { Category } from './category.model';
import { RequestOrder } from './request-order.model';
import { ExpoTokenNotification } from './expo-token-notification.model';

export const model = {
    Buyer,
    Seller,
    Product,
    Category,
    RequestOrder,
    ExpoTokenNotification
}