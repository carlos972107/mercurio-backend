'use strict';

import { Schema, model, Document } from 'mongoose';
import * as bcrypt from 'bcrypt';
import { ENV } from '../config';
import * as jsonwebtoken from 'jsonwebtoken';
const emailRegex = /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i;

const { Mixed } = Schema.Types;

export interface IUser extends Document{
    email: string,
    password: string,
    role: string,
    state: string,
    comparePassword(password:string): boolean
    token(user): string
}

const BuyerSchema = new Schema({
    name: {
        type: String
    },
    lastname: {
        type: String
    },
    phone: {
        type: String
    },
    mobile: {
        type: String
    },
    email: {
        type: String,
        validate: {
            validator: (v) => {
              return emailRegex.test(v);
            },
            message: 'Correo electrónico no válido'
        }
    },
    password: {
        type: String,
        required: true
    },
    address: {
        lat: String,
        lng: String,
        zoom: Number
    },
    physicalAddress: {
        type: String,
        required: true
    },
    addressMaps: Mixed,
    photo_url: {
        type: String
    },
    role: {
        type: String,
        required: true
    },
    state: {
        type: String
    },
    createAt: {
        type: String,
        default: new Date().toLocaleString()
    },
    updateAt: {
        type: String,
        default: new Date().toLocaleString()
    }
});

BuyerSchema.pre('save',async function(next){
    const self = this as IUser;

    if (!this.isModified('password')) {
        return next();
    }
    
    const doc = await this.constructor.find({
        email: self.email
    }).exec();

    if(doc.length) throw 'error correo en uso';

    try{
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(self.password, salt);
        self.password = hash;
        return next(null, this);
    }catch(err){
        throw err;
    }
});

BuyerSchema.pre(['findByIdAndUpdate', 'findOneAndUpdate'], async function(next){
    const self = this.getUpdate();
    self.updateAt = new Date().toLocaleString();
    if (self.password) {
        try{
            const salt = await bcrypt.genSalt(10);
            const hash = await bcrypt.hash(self.password, salt);
            self.password = hash;
            return next(null, this);
        }catch(err){
            throw err;
        }
    }
})

BuyerSchema.methods.comparePassword = function(candidatePassword){
    return bcrypt.compare(candidatePassword, this.password);
};

BuyerSchema.methods.token = function(user){
    const payload = user;
    return 'Bearer ' + jsonwebtoken.sign(payload.toJSON(),
        ENV.JWT_ENCRYPTION, { expiresIn: ENV.JWT_EXPIRATION });
}

export const Buyer = model<IUser>('Buyer', BuyerSchema);