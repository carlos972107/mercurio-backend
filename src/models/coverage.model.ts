'use strict';

import { Schema, model, Document } from 'mongoose';

const { Mixed } = Schema.Types;

const CoverageSchema = new Schema({
    code: {
        type: Number,
        required: true
    },
    municipality: {
        type: String
    },
    name: {
        type: String,
        required: true
    },
    coordinateMaps: {
        lat: Number,
        lng: Number,
        zoom: Number,
        coordinates: Array
    },
    createAt: {
        type: String,
        default: new Date().toLocaleString()
    },
    updateAt: {
        type: String,
        default: new Date().toLocaleString()
    }
});

export const Coverage = model('Coverage', CoverageSchema);