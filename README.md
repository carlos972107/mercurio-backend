# Mercurio Backend

back end desarrollo de la APi que proporciona el uso de la base de datos de un grupo de aplicaciones hibridas para la administracion de recursos como compra, venta y transporte de diferentes establecimientos.

las solicitudes de la base de datos se podran realizar a tráves del playground de Graphql que nos proporciona las herramientas para poder realizar las consultas a la Api sin ningun contratiempo, podemos realizar dos tipo de consutas que son las *Mutation* o *Query*, hasta el momento, como por ejemplo:
```
Mutation{
  loginUser(Correo: "tucorreo@tucorreo.com", Contrasena: "tu_contrasena")
}
```

### instalacion Local

principalmente se debe tener el cuenta que cumplimos ya con la previa instalacion del servidor NodeJS si no es asi podemos dirigirnos a esta URL https://nodejs.org/es/ y descargar el instaldor para sus sistema y realizar la instalacion adecuada.

una vez ya hayamos instalado el servidor node js podemos realizar la clonacion del respositorio joi-backend en la rama con nombre master a tráves de la sentencia:
```
git clone https://gitlab.com/
```

una vez clonado el repositorio vamos a instalar las dependencias desde la consola de usuario dentro de la carpeta del respositorio a tráves de la sentencia:
```
npm install
```
luego de la instalacion satisfactoria iniciamos el servidor de prueba con la sentencia
```
npm run debug
```
e ingresamos al navegador con la URL https://localhost:4004/graphql

Listo tenemos el servidor en funcionamiento, ya podemos realizar nuestras consutlas con GraphQL

## Authors

* **Carlos Sanchez** - *Fullstack javascript* - [Joi](https://github.com)

## License

Este proyecto trabaja bajo las licencia MIT License - ver en [LICENSE.md](LICENSE.md) archivo detallado.
